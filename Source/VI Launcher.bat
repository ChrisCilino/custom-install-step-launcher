@ECHO Off
SET /A LVYear=%1
SET /A Bitness=%2
Set Parameters=%*
Set CurDir=%CD%
If %Bitness% EQU 32 (SET Architecture=Program Files ^(x86^)) else (SET Architecture=Program Files)
Set LVDir=C:\%Architecture%\National Instruments\LabVIEW %LVYear%
Set LVExePath="%LVDir%\LabVIEW.exe"
Set LauncherVIPath="%LVDir%\user.lib\_PetranWay\Custom Install Step Launcher\Source\Launch Install VI.vi"
LabVIEWCLI -LabVIEWPath %LVExePath% -PortNumber 3363 -OperationName RunVI -VIPath %LauncherVIPath% %Parameters% "Current Directory =%~dp0\"
